<?php

require '../../vendor/autoload.php';
require '../../config/database.php';
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;
use App\entities\User;
use Sirius\Validation\Validator;


class Registrar{


    function registrarUsuario(Request $request){
       
        
        $user = new User();
        $validator = new Validator();
        $users = User::where('doc','=', $request->get('doc'))->orWhere('email', '=', $request->get('email'))->first();
        $datos = json_decode($users, true);
        if(!empty($datos['email']) && !empty($datos['email'])){
            header("Location: ../../registrar");
        }else{
            $datos = array(
                'name' => $request->get('name'),
                'doc'  => $request->get('doc'),
                'email'=> $request->get('email'),
                'password' => $request->get('password')
            );
            $validator->add(
                array(
                    'name:Nombre' => 'required | minlength(min=3)',
                    'email:Tu email' => 'required | email',
                    'doc:Tu Dcocumento' => 'required ',
                    'password' => 'required | minlength(min=6)'
                )
            );

            if ($validator->validate($datos)) {

                $user->nombre = $request->get('name');
                $user->doc = $request->get('doc');
                $user->email = $request->get('email');
                $user->pais = $request->get('pais');
                $user->password = $request->get('password');
                if($user->save()){
                    header("Location: ../../");
                }else{
                    header("Location: ../../registrar");
                }
                
            }else{
                header("Location: ../../registrar");
            }
       
        }
    }
}
?>