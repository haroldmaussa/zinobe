-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2020 a las 21:12:22
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zinobe`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `simits`
--

CREATE TABLE `simits` (
  `id` int(11) NOT NULL,
  `estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `numeropaz` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `numerocomparendos` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `suspencion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `simits`
--

INSERT INTO `simits` (`id`, `estado`, `numeropaz`, `numerocomparendos`, `suspencion`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'SI', '429895198062', '0', 'NO', 9, '2020-02-08 02:39:39', '2020-02-08 02:39:39'),
(2, 'SI', '429896325065', '0', 'NO', 1, '2020-02-08 02:45:22', '2020-02-08 02:45:22'),
(3, 'NO', 'NO', 'NO', 'NO', 10, '2020-02-08 22:57:49', '2020-02-08 22:57:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sisbens`
--

CREATE TABLE `sisbens` (
  `id` int(11) NOT NULL,
  `puntaje` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `departamento` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `municipio` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigomunicipio` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fechaingreso` int(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sisbens`
--

INSERT INTO `sisbens` (`id`, `puntaje`, `departamento`, `municipio`, `codigomunicipio`, `fechaingreso`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '22,24', 'Córdoba', 'Valencia', '23855', 16, 1, '2020-02-07 22:23:09', '2020-02-07 22:23:09'),
(10, '22,24', 'Córdoba', 'Valencia', '23855', 16, 9, '2020-02-08 01:34:20', '2020-02-08 01:34:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `doc` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `doc`, `email`, `pais`, `password`, `created_at`, `updated_at`) VALUES
(1, 'awgs', 1068811859, 'd@k.com', 'Bahamas', '4', '2020-02-06 19:27:42', '2020-02-06 19:27:42'),
(9, 'fff', 50859314, 'h@h.com', 'Azerbaijan', '1', '2020-02-07 06:13:43', '2020-02-07 06:13:43'),
(10, 'hmaussa', 1234567, 'haroldmaussa@gmail.com', 'Colombia', '123', '2020-02-08 20:25:55', '2020-02-08 20:25:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `simits`
--
ALTER TABLE `simits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sisbens`
--
ALTER TABLE `sisbens`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `doc` (`doc`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `simits`
--
ALTER TABLE `simits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sisbens`
--
ALTER TABLE `sisbens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
